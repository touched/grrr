<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<style>
.center_div{
    margin: 0 auto;
    width:80% /* value of your choice which suits your alignment */
}
</style>
</head>
<body>
<br/>
<div class="container center_div">
<div class="jumbotron">
        <h1 class="display-3"><b>BlackOutMC whitelist</b></h1>
        <p class="lead">Since it is impossible for a human to handle the amount of whitelisted needed to be done, we let applications automate it for us!</p>
        <p><a class="btn btn-lg btn-success" href="https://blackoutmc.net" role="button">Main Website</a></p>
		<form action="check.php" method="post">
  <div class="form-group">
    <label for="username">Username </label>
    <input type="text" class="form-control" name="username" id="username" aria-describedby="userHelp" placeholder="Minecraft Username">
	<br/>
    <small id="userHelp" class="form-text text-muted">We'll never share your Username and/or UUID with anyone else.</small>
	<br/>
	<button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>
      </div>
	  </div>


 <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>

